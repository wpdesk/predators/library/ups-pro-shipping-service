## [2.0.2] - 2024-05-09
### Fixed
- Minimal invoice line total value

## [2.0.1] - 2023-06-13
### Fixed
- REST API libraries

## [2.0.0] - 2023-06-02
### Added
- REST API support

## [1.8.3] - 2023-03-10
### Fixed
- Fatal error in surepost service

## [1.8.2] - 2023-03-01
### Added
- Simple Rate

## [1.7.2] - 2023-01-05
### Added
- Custom Origin instance settings location

## [1.7.1] - 2023-01-02
### Added
- Custom Origin instance settings for One Rate services

## [1.7.0] - 2022-12-29
### Added
- Custom Origin instance settings

## [1.6.3] - 2022-12-01
### Fixed
- Delivery confirmation values

## [1.6.2] - 2022-06-20
### Fixed
- Delivery confirmation in Canada, USA and Puerto Rico on package level (UPS documentation)

## [1.6.0] - 2021-11-05
### Changed
- surepost moved to separate service

## [1.5.3] - 2021-10-29
### Fixed
- surepost services

## [1.5.2] - 2021-07-28
### Fixed
- texts and translations

## [1.5.0] - 2021-07-26
### Added
- delivery confirmation

## [1.4.1] - 2021-03-02
### Fixed
- cleanup after pickup type moved to free 

## [1.4.0] - 2021-03-02
### Removed
- pickup type moved to free version

## [1.3.0] - 2021-01-05
### Added
- blackout lead days

## [1.2.6] - 2020-07-23
### Added
- custom independent settings for pickup_type fields
- custom independent settings for rate_adjustments_title field

## [1.2.5] - 2020-01-27
### Changed
- create_sender method

## [1.2.4] - 2020-01-14
### Fixed
- time in transit is required

## [1.2.3] - 2020-01-14
### Fixed
- estimated delivery settings

## [1.2.2] - 2020-01-14
### Added
- InvoiceLineTotal in rate request

## [1.2.1] - 2020-01-14
### Fixed
- number instead of decimal fields in settings

## [1.2.0] - 2020-01-11
### Added
- multi currency support
- packer support

## [1.1.0] - 2020-01-08
### Added
- filtered rates

## [1.0.1] - 2019-12-30
### Added
- delivery dates

## [1.0.0] - 2019-12-18
### Added
- initial version
