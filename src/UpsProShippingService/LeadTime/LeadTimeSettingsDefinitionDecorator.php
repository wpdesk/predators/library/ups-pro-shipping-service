<?php
/**
 * Decorator for lead time.
 *
 * @package WPDesk\UpsProShippingService\LeadTime
 */

namespace WPDesk\UpsProShippingService\LeadTime;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\UpsProShippingService\MaximumTransitTime\MaximumTransitTimeSettingsDefinitionDecorator;

/**
 * Can decorate settings for lead time field.
 */
class LeadTimeSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const OPTION_LEAD_TIME = 'lead_time';

	public function __construct( SettingsDefinition $ups_settings_definition ) {
		parent::__construct(
			$ups_settings_definition,
			MaximumTransitTimeSettingsDefinitionDecorator::OPTION_MAXIMUM_TRANSIT_TIME,
			self::OPTION_LEAD_TIME,
			array(
				'title'             => __( 'Lead Time', 'ups-pro-shipping-service' ),
				'type'              => 'number',
				'description'       => __(
					'Lead Time is used to define how many days are required to prepare an order for shipment. The delivery date or time will be updated for the selected number of days.',
					'ups-pro-shipping-service'
				),
				'desc_tip'          => true,
				'default'           => '0',
				'custom_attributes' => array(
					'min' => 0,
				),
			)
		);
	}

}
