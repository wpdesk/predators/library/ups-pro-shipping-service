<?php
/**
 * Decorator for maximum transit time.
 *
 * @package WPDesk\UpsProShippingService\MaximumTransitTime
 */

namespace WPDesk\UpsProShippingService\MaximumTransitTime;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\UpsProShippingService\DestinationAddressType\EstimatedDeliverySettingsDefinitionDecorator;

/**
 * Can decorate settings for maximum transit time field.
 */
class MaximumTransitTimeSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const OPTION_MAXIMUM_TRANSIT_TIME = 'maximum_transit_time';

	public function __construct( SettingsDefinition $ups_settings_definition ) {
		parent::__construct(
			$ups_settings_definition,
			EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES,
			self::OPTION_MAXIMUM_TRANSIT_TIME,
			array(
				'title'             => __( 'Maximum Time in Transit', 'ups-pro-shipping-service' ),
				'type'              => 'number',
				'description'       => __( 'Maximum Time in Transit is used to define the number of maximum days goods can be in transit. Only days in transit are counted. This is often used for perishable goods.', 'ups-pro-shipping-service' ),
				'desc_tip'          => true,
				'custom_attributes' => array(
					'min'  => '0',
				)
			)
		);
	}

}
