<?php
/**
 * Settings definition.
 *
 * @package WPDesk\UpsProShippingService
 */

namespace WPDesk\UpsProShippingService;

use WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException;
use WPDesk\AbstractShipping\Settings\SettingsDecorators\BlackoutLeadDaysSettingsDefinitionDecoratorFactory;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\UpsProShippingService\CollectionPointFlatRate\CollectionPointFlatRateSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\CutoffTime\CutoffTimeSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\DatesAndTimes\DatesAndTimesSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\DeliveryConfirmation\DeliveryConfirmationSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\DestinationAddressType\DestinationAddressTypeSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\DestinationAddressType\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\InstanceCustomOrigin\CustomOriginSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\InstanceCustomOrigin\InstanceCustomOriginSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\LeadTime\LeadTimeSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\MaximumTransitTime\MaximumTransitTimeSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\RateAdjustmentsTitle\RateAdjustmentsTitleSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\PickupType\HandlingFeesSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\SimpleRate\SimpleRateSettingsDefinitionDecorator;
use WPDesk\UpsShippingService\UpsSettingsDefinition;

/**
 * Settings definitions.
 */
class UpsProSettingsDefinition extends SettingsDefinition {

	/**
	 * UPS settings definition.
	 *
	 * @var UpsSettingsDefinition
	 */
	private $ups_settings_definition;

	/**
	 * UpsProSettingsDefinition constructor.
	 *
	 * @param UpsSettingsDefinition $ups_settings_definition UPS settings definition.
	 */
	public function __construct( UpsSettingsDefinition $ups_settings_definition ) {
        $ups_settings_definition       = new RateAdjustmentsTitleSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new DestinationAddressTypeSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new HandlingFeesSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new DatesAndTimesSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new EstimatedDeliverySettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new MaximumTransitTimeSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new LeadTimeSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new CutoffTimeSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = ( new BlackoutLeadDaysSettingsDefinitionDecoratorFactory() )->create_decorator( $ups_settings_definition, CutoffTimeSettingsDefinitionDecorator::OPTION_CUTOFF_TIME, false );
        $ups_settings_definition       = new CollectionPointFlatRateSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new DeliveryConfirmationSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new CustomOriginSettingsDefinitionDecorator( $ups_settings_definition, BlackoutLeadDaysSettingsDefinitionDecoratorFactory::OPTION_ID );
        $ups_settings_definition       = new InstanceCustomOriginSettingsDefinitionDecorator( $ups_settings_definition );
        $ups_settings_definition       = new SimpleRateSettingsDefinitionDecorator( $ups_settings_definition );
        $this->ups_settings_definition = $ups_settings_definition;
    }

	/**
	 * Get form fields.
	 *
	 * @return array
	 *
	 * @throws SettingsFieldNotExistsException .
	 */
	public function get_form_fields() {
		return $this->ups_settings_definition->get_form_fields();
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->ups_settings_definition->validate_settings( $settings );
	}
}
