<?php

namespace WPDesk\UpsProShippingService\UpsApi;

use Psr\Log\LoggerInterface;
use Ups\Entity\RateRequest;
use Ups\Entity\RateResponse;
use Ups\Exception\InvalidResponseException;
use Ups\RateTimeInTransit;
use WPDesk\AbstractShipping\Exception\RateException;
use WPDesk\UpsShippingService\UpsApi\UpsRateReplyInterpretation;
use WPDesk\UpsShippingService\UpsApi\UpsSenderSingleRate;

class UpsProSenderSingleRate extends UpsSenderSingleRate {

	/**
	 * Request time in transit.
	 *
	 * @var bool
	 */
	private $request_time_in_transit;

	public function __construct(
		$access_key,
		$user_id,
		$password,
        $service_code,
		LoggerInterface $logger,
		$is_testing = false,
		$is_tax_enabled = true,
		$request_time_in_transit = true
	) {
		parent::__construct( $access_key, $user_id, $password, $service_code, $logger, $is_testing, $is_tax_enabled );
		$this->request_time_in_transit = $request_time_in_transit;
	}

	/**
	 * Send request.
	 *
	 * @param RateRequest $request UPS request.
	 *
	 * @return RateResponse
	 *
	 * @throws \Exception .
	 * @throws RateException .
	 */
	public function send( RateRequest $request ) {

		$rate = new RateTimeInTransit( $this->get_access_key(), $this->get_user_id(), $this->get_password(), $this->is_testing(), $this->get_logger() );
		try {
			if ( $this->request_time_in_transit ) {
				$reply = $rate->getRateTimeInTransit( $request );
			} else {
				$reply = $rate->getRate( $request );
			}
		} catch ( InvalidResponseException $e ) {
			throw new RateException( $e->getMessage(), [ 'exception' => $e->getCode() ] ); //phpcs:ignore
		}
		$rate_interpretation = new UpsRateReplyInterpretation( $reply, $this->is_tax_enabled() );

		if ( $rate_interpretation->has_reply_error() ) {
			throw new RateException( $rate_interpretation->get_reply_message(), [ 'response' => $reply ] ); //phpcs:ignore
		}

		return $reply;
	}

}
