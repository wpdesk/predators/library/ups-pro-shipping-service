<?php

namespace WPDesk\UpsProShippingService;

use Ups\Entity\RateResponse;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\AbstractShipping\Shipment\Shipment;
use WPDesk\AbstractShipping\ShippingServiceCapability\CanPack;
use WPDesk\UpsProShippingService\DestinationAddressType\EstimatedDeliverySettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\UpsApi\UpsProRateRequestBuilder;
use WPDesk\UpsProShippingService\UpsApi\UpsProSender;
use WPDesk\UpsProShippingService\UpsApi\UpsProSenderSingleRate;
use WPDesk\UpsProShippingService\UpsApi\UpsRestApi\UpsProRestApiSender;
use WPDesk\UpsProShippingService\UpsApi\UpsRestApi\UpsProRestApiSenderSingleRate;
use WPDesk\UpsShippingService\UpsApi\UpsProRateReplyInterpretation;
use WPDesk\UpsShippingService\UpsApi\UpsRateReplyInterpretation;
use WPDesk\UpsShippingService\UpsApi\UpsRateRequestBuilder;
use WPDesk\UpsShippingService\UpsApi\UpsSender;
use WPDesk\UpsShippingService\UpsSettingsDefinition;
use WPDesk\UpsShippingService\UpsSurepostShippingService;
use WPDesk\WooCommerceShipping\ShopSettings;

/**
 * Shipping service.
 */
class UpsProSurepostShippingService extends UpsSurepostShippingService implements CanPack {

	/**
	 * Get settings
	 *
	 * @return UpsProSurepostSettingsDefinition
	 */
	public function get_settings_definition() {
		return new UpsProSurepostSettingsDefinition( parent::get_settings_definition() );
	}

	/**
	 * Create rate request builder.
	 *
	 * @param SettingsValues $settings .
	 * @param Shipment       $shipment .
	 * @param ShopSettings   $shop_settings .
	 *
	 * @return UpsRateRequestBuilder
	 */
	protected function create_rate_request_builder( SettingsValues $settings, Shipment $shipment, ShopSettings $shop_settings ) {
		return new UpsProRateRequestBuilder( $settings, $shipment, $shop_settings, $this->get_logger() );
	}

	/**
	 * Create sender.
	 *
	 * @param SettingsValues $settings Settings Values.
	 *
	 * @return UpsSender
	 */
    protected function create_single_rate_sender( SettingsValues $settings, string $surepost_service_code )
    {
        $delivery_dates          = $settings->get_value(
            EstimatedDeliverySettingsDefinitionDecorator::OPTION_DELIVERY_DATES,
            EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE
        );
        $request_time_in_transit = EstimatedDeliverySettingsDefinitionDecorator::OPTION_NONE !== $delivery_dates;
        if ($settings->get_value(UpsSettingsDefinition::API_TYPE,
                UpsSettingsDefinition::API_TYPE_XML) === UpsSettingsDefinition::API_TYPE_REST) {
            return new UpsProRestApiSenderSingleRate(
                $this->rest_api_client,
                $surepost_service_code,
                $this->get_logger(),
                $this->is_testing($settings),
                $this->get_shop_settings()->is_tax_enabled(),
                $request_time_in_transit
            );
        } else {
            return new UpsProSenderSingleRate(
                $settings->get_value(UpsSettingsDefinition::ACCESS_KEY),
                $settings->get_value(UpsSettingsDefinition::USER_ID),
                $settings->get_value(UpsSettingsDefinition::PASSWORD),
                $surepost_service_code,
                $this->get_logger(),
                $this->is_testing($settings),
                $this->get_shop_settings()->is_tax_enabled(),
                $request_time_in_transit
            );
        }
    }


	/**
	 * Create reply interpretation.
	 *
	 * @param RateResponse   $response .
	 * @param ShopSettings   $shop_settings .
	 * @param SettingsValues $settings .
	 *
	 * @return UpsRateReplyInterpretation
	 */
	protected function create_reply_interpretation( RateResponse $response, $shop_settings, $settings ) {
	    return new UpsProRateReplyInterpretation(
			$response,
			$shop_settings->is_tax_enabled()
		);
	}

    /**
     * Verify currency.
     *
     * @param string $default_shop_currency Shop currency.
     * @param string $checkout_currency Checkout currency.
     *
     * @return void
     */
    protected function verify_currency( $default_shop_currency, $checkout_currency ) {
        // Do nothing. We currently support multi currency.
    }


}
