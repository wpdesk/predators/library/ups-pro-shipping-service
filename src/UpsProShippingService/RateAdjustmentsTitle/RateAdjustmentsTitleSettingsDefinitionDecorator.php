<?php
/**
 * Decorator for rate adjustments title field.
 *
 * @package WPDesk\UpsProShippingService\PickupType
 */

namespace WPDesk\UpsProShippingService\RateAdjustmentsTitle;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\UpsShippingService\UpsSettingsDefinition;

/**
 * Can decorate settings by adding pickup type field.
 */
class RateAdjustmentsTitleSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const RATE_ADJUSTMENTS_TITLE = 'rate_adjustments_title';

    /**
     * @param SettingsDefinition $ups_settings_definition
     * @param string $field_id_after
     */
	public function __construct( SettingsDefinition $ups_settings_definition, $field_id_after = UpsSettingsDefinition::SERVICES ) {
		parent::__construct(
			$ups_settings_definition,
			$field_id_after,
			 self::RATE_ADJUSTMENTS_TITLE,
			$this->get_field_settings()
		);
	}

	/**
	 * Get field settings.
	 *
	 * @return array .
	 */
	private function get_field_settings() {
		return array(
			'title'       => __( 'Rates Adjustments', 'ups-pro-shipping-service' ),
			'description' => sprintf( __( 'Adjust these settings to get more accurate rates. Read %swhat affects the UPS rates in UPS WooCommerce plugin →%s', 'ups-pro-shipping-service' ),
				sprintf( '<a href="%s" target="_blank">', __( 'https://wpde.sk/ups-pro-rates-eng/', 'ups-pro-shipping-service' ) ),
				'</a>',
				),
			'type'        => 'title',
		);
	}

	/**
	 * Replaces settings field from free version.
	 *
	 * @return array .
	 * @throws \WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException
	 */
	public function get_form_fields() {
		$form_fields = parent::get_form_fields();
		$form_fields[ self::RATE_ADJUSTMENTS_TITLE ] = $this->get_field_settings();

		return $form_fields;
	}

}
