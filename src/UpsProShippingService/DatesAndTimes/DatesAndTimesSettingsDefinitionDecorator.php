<?php
/**
 * Decorator for dates and times section.
 *
 * @package WPDesk\UpsProShippingService\DatesAndTimes
 */

namespace WPDesk\UpsProShippingService\DatesAndTimes;

use WPDesk\AbstractShipping\Settings\DefinitionModifier\SettingsDefinitionModifierAfter;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\UpsProShippingService\PickupType\HandlingFeesSettingsDefinitionDecorator;

/**
 * Can decorate settings by adding handling fees field.
 */
class DatesAndTimesSettingsDefinitionDecorator extends SettingsDefinitionModifierAfter {

	const DATES_AND_TIMES_TITLE = 'dates_and_times_title';

	public function __construct( SettingsDefinition $ups_settings_definition ) {
		parent::__construct(
			$ups_settings_definition,
			HandlingFeesSettingsDefinitionDecorator::HANDLING_FEES,
			self::DATES_AND_TIMES_TITLE,
			array(
				'title'       => __( 'Dates & Time', 'ups-pro-shipping-service' ),
				'description' => __( 'Manage services\' dates information.', 'ups-pro-shipping-service' ),
				'type'        => 'title',
			)
		);
	}

}