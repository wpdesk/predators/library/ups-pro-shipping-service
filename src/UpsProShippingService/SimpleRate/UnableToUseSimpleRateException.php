<?php

namespace WPDesk\UpsProShippingService\SimpleRate;

/**
 * Unable to use simple rate.
 */
class UnableToUseSimpleRateException extends \RuntimeException {
}
