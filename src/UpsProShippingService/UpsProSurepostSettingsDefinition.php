<?php

namespace WPDesk\UpsProShippingService;

use WPDesk\AbstractShipping\Exception\SettingsFieldNotExistsException;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\Settings\SettingsValues;
use WPDesk\UpsProShippingService\DeliveryConfirmation\DeliveryConfirmationSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\DestinationAddressType\DestinationAddressTypeSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\InstanceCustomOrigin\CustomOriginSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\InstanceCustomOrigin\InstanceCustomOriginSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\RateAdjustmentsTitle\RateAdjustmentsTitleSettingsDefinitionDecorator;
use WPDesk\UpsProShippingService\PickupType\HandlingFeesSettingsDefinitionDecorator;
use WPDesk\UpsShippingService\UpsSettingsDefinition;
use WPDesk\UpsShippingService\UpsSurepostSettingsDefinition;

/**
 * Settings definitions.
 */
class UpsProSurepostSettingsDefinition extends SettingsDefinition {

	/**
	 * UPS settings definition.
	 *
	 * @var UpsSettingsDefinition
	 */
	private $ups_surepost_settings_definition;

	/**
	 * UpsProSettingsDefinition constructor.
	 *
	 * @param UpsSurepostSettingsDefinition $ups_surepost_settings_definition UPS settings definition.
	 */
	public function __construct( UpsSurepostSettingsDefinition $ups_surepost_settings_definition ) {
        $ups_surepost_settings_definition       = new RateAdjustmentsTitleSettingsDefinitionDecorator( $ups_surepost_settings_definition, UpsSurepostSettingsDefinition::SUREPOST_SERVICES );
        $ups_surepost_settings_definition       = new DestinationAddressTypeSettingsDefinitionDecorator( $ups_surepost_settings_definition );
        $ups_surepost_settings_definition       = new HandlingFeesSettingsDefinitionDecorator( $ups_surepost_settings_definition );
        $ups_surepost_settings_definition       = new CustomOriginSettingsDefinitionDecorator( $ups_surepost_settings_definition , HandlingFeesSettingsDefinitionDecorator::HANDLING_FEES );
        $ups_surepost_settings_definition       = new InstanceCustomOriginSettingsDefinitionDecorator( $ups_surepost_settings_definition );
        $this->ups_surepost_settings_definition = $ups_surepost_settings_definition;
    }

	/**
	 * Get form fields.
	 *
	 * @return array
	 *
	 * @throws SettingsFieldNotExistsException .
	 */
	public function get_form_fields() {
		return $this->ups_surepost_settings_definition->get_form_fields();
	}

	/**
	 * Validate settings.
	 *
	 * @param SettingsValues $settings Settings.
	 *
	 * @return bool
	 */
	public function validate_settings( SettingsValues $settings ) {
		return $this->ups_surepost_settings_definition->validate_settings( $settings );
	}
}
